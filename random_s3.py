import boto3
import os
from datetime import datetime

filename = str(datetime.now()) + '.timestamp'

with open(filename, 'w') as f:
    f.write("hello world")
    f.write("shoop")

s3 = boto3.client('s3', region_name='eu-north-1')

s3.upload_file(filename, 'axis-sandbox-cicd', filename)

os.remove(filename)